/**
 * Homework 3
 * TCSS 422
 * Operating Systems
 * Christopher Helmer
 *
 * This program analyzes all bmp images in a directory and calculates the largest
 * rectangle in each.
 */
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <pthread.h>
#include "analyzer.h"

int num_bmps;

/**
 * This struct holds all data for an individual pixel including where the rectangle
 * it belongs to started and the area of that rectangle up until this pixel
 */
struct pixel {
	int color;
	int x;
	int y;
	int start_x, start_y, area;
};

/**
 * This struct holds all the data for an entire image
 */
struct image {
	char *path;
	int height;
	int width;
	struct pixel *pixArray;
};

/**
 * Struct that holds the arguments passed into the copyThread thread.
 */
typedef struct  {
	char *directory;
	struct image *images;
}BMPcopy;

/**
 * Struct that holds the arguments passed into the thread
 * handling the image analysis
 */
typedef struct {
	struct image *images;
	struct ImageInfo *analyzedImages;
	int k, i, thread_limit;
}bmp_analyze;

/**
 * Struct that stores the pixel that is at the top left of the largest rectangle
 */
typedef struct {
	struct pixel max_pix;
}max_pixel;

/**
 * Thread that copies image info into an array in memory
 */
void copyThread(BMPcopy *copy) {

	int i, j, k, l, m;

	//create a directory interface and entity
	struct dirent *dirEntity;
	DIR *dir;

	char *direct = (char *)malloc(200 * sizeof(char));
	k = 0;
	while(copy->directory[k] != '\0') {
		direct[k] = copy->directory[k];
		k++;
	}
	direct[k] = '\0';

	//open the directory into the directory interface
	dir = opendir(direct);
	//If the directory is NULL...
	if(dir == NULL) {
		printf("Directory %s does not exist.", direct);

	}
	i = 0;
	while((dirEntity = readdir(dir)) != NULL) {

		j = 0;
		k = 0;

		char *filePath = (char *)malloc(300 * sizeof(char));
		//From the directory name and file name, create a path.
		while(direct[j] != '\0') {
			filePath[j] = direct[j];
			j++;
		}
		filePath[j] = '/';
		j++;
		while(dirEntity->d_name[k] != '\0') {
			filePath[j] = dirEntity->d_name[k];
			j++;
			k++;
		}
		filePath[j] = '\0';

		//Open the file in binary read mode
		FILE *binFile;
		binFile = fopen(filePath, "rb");
		if(binFile == NULL) {
			printf("Error: Unable to open %s.\n", filePath);
		}


		//read the header of the file
		unsigned char *header = (unsigned char *)malloc(2 * sizeof(unsigned char));
		fseek(binFile, 0, SEEK_SET);
		fread(header, 0x02, 0x02, binFile);

		//if the header == BM (if it's a bitmap)
		if(header[0] == 0x42 && header[1] == 0x4d) {
			num_bmps++;
			//get the offset for the beginning of the pixel array
			int *ArrayOffset = (int *)malloc(sizeof(int));
			fseek(binFile, 0xA, SEEK_SET);
			fread(ArrayOffset, sizeof(char), sizeof(char), binFile);
			//printf("%d\n", *ArrayOffset );

			//get the width of the image
			int *width = (int *)malloc(1 * sizeof(int));
			fseek(binFile, 0x12, SEEK_SET);
			fread(width, 4 * sizeof(char), 4 * sizeof(char), binFile);
			//printf("%x\n", *width);

			//determine the pad at the end of each row.
			const int pad = (3 * *width) % 4;

			//get the height of the image
			int *height = (int *)malloc(1 * sizeof(int));
			fseek(binFile, 0x16, SEEK_SET);
			fread(height, 4 * sizeof(char), 4 * sizeof(char), binFile);
			//printf("%x\n", *height);

			//field for pixel color
			int *color = (int *)malloc(sizeof(int));
			//new image array to fit pixel data into
			copy->images[i].pixArray = (struct pixel *)malloc(*height * *width * sizeof(struct pixel));
			copy->images[i].height = *height;
			copy->images[i].width = *width;

			//copy the pixel array into memory
			for(l = 0; l < *height; l++){
				for(m = 0; m < *width; m++) {
					struct pixel pix;
					//get the pixel data at the current offset
					fseek(binFile, *ArrayOffset, SEEK_SET);
					fread(color, sizeof(int), sizeof(int), binFile);
					//bitmask because we only care about the three bytes that make up the color
					*color &= 0x00ffffff;
					//fill pixel data
					pix.color = *color;
					pix.x = m;
					pix.y = l;
					pix.start_x = m;
					pix.start_y = l;
					pix.area = 1;
					//put pixel in image array
					copy->images[i].pixArray[*width * l + m] = pix;
					//move the offset ahead three bytes
					*ArrayOffset += 3;
					*color = 0;
				}
				//move the offset ahead by the size of the row pad
				*ArrayOffset += pad;
			}

			//copy the path of the file into the image struct
			copy->images[i].path = (char *)malloc(100 * sizeof(char));
			k = 0;
			while(filePath[k] != '\0') {
				copy->images[i].path[k] = filePath[k];
				k++;
			}
			copy->images[i].path[k] = '\0';

			//free up all the memory that was used
			free(color);
			free(ArrayOffset);
			free(height);
			free(width);

			free(filePath);
			i++;
		}
	}
	free(direct);
}

/*
 * Function that analyzes images.
 * The algorithm I designed is dynamic. It starts from every corner and
 * marks pixels with the area of the rectangle they belong to up until that pixel, and it marks the
 * pixel with the starting coordinates of that rectangle. Because rectangles may be part of
 * other rectangles, it is important that we go from each corner so as to not miss a larger
 * rectangle than what we may have found by only starting from one corner.
 */
 void analyze_bmp_now(struct ImageInfo *analyzedImages, struct image *images, int k) {
	int l, m;
	//set the max pixel
	max_pixel max;
	max.max_pix.area = 1;
	max.max_pix.color = 0;
	max.max_pix.start_x = 1;
	max.max_pix.start_y = 1;
	max.max_pix.x = 1;
	max.max_pix.y = 1;

	//create a ImageInfo struct for this image
	//struct ImageInfo image_inf;
	analyzedImages[k].path = images[k].path;

	//scan from the bottom right of the picture
	for(l = 0; l < images[k].height; l++) {
		for(m = 0; m < images[k].width; m++) {
			//If the pixel to the left and below are the same color
			if(m - 1 >= 0 && l - 1 >= 0) {

				//if the color matches below and behind
				if(images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

					if(images[k].pixArray[images[k].width * (l - 1) + m].area <= images[k].pixArray[images[k].width * l - 1 + m - 1].area){
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);
					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);
					}


					//if behind is not a match, but below is
				} else if (images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * l + m - 1].color
						&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

					//if the starting_x of the one below is not less than the current x position
					if(images[k].pixArray[images[k].width * (l - 1) + m].start_x == images[k].pixArray[images[k].width * l + m].x) {
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);
					}

					//if behind is a match, but below is not and the start_y of the one behind equal to the current y postition
				} else if (images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color
						&& images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * (l - 1) + m].color
						&& images[k].pixArray[images[k].width * l + m - 1].start_y == images[k].pixArray[images[k].width * l + m].y) {

					//we are in the same rectangle, so we have the same starting point
					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

				} else {

					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

				}

			//if behind is a match and you're in the first row
			} else if(m - 1 >= 0 && l - 1 < 0
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

			//if below is a match and you're in the first column
			} else if (m - 1 < 0 && l - 1 >= 0
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

			}
			//If this pixel has an equal area to the max pixel and it's further right or further up, make it the max
			if((images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].x > max.max_pix.x)
					|| (images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].y > max.max_pix.y)) {

				max.max_pix = images[k].pixArray[images[k].width * l + m];

			}
			//If the area at this pixel is greater than the current largest pixel, make the max pixel be this pixel
			if(images[k].pixArray[images[k].width * l + m].area > max.max_pix.area) {
				max.max_pix = images[k].pixArray[images[k].width * l + m];
			}
		}
	}
	//reset all the pixels back to original values
	for(l = 0; l < images[k].height; l++) {
		for(m = 0; m < images[k].width; m++) {
			images[k].pixArray[images[k].width * l + m].area = 1;
			images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
			images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
		}
	}
	//scan the picture from the top left
	for(l = images[k].height - 1; l >= 0; l--) {
		for(m = 0; m < images[k].width; m++) {
			//If the pixel to the left and below are the same color
			if(m - 1 >= 0 && l + 1 < images[k].height) {

				//if the color matches below and behind
				if(images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

					if(images[k].pixArray[images[k].width * (l + 1) + m].area <= images[k].pixArray[images[k].width * l + m - 1].area){
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					}


					//if behind is not a match, but below is
				} else if (images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * l + m - 1].color
						&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

					//if the starting_x of the one below is not less than the current x position
					if(images[k].pixArray[images[k].width * (l + 1) + m].start_x == images[k].pixArray[images[k].width * l + m].x) {
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					}

					//if behind is a match, but below is not and the start_y of the one behind equal to the current y postition
				} else if (images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color
						&& images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * (l + 1) + m].color
						&& images[k].pixArray[images[k].width * l + m - 1].start_y == images[k].pixArray[images[k].width * l + m].y) {

					//we are in the same rectangle, so we have the same starting point
					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

				} else {

					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

				}

			//if behind is a match and you're in the first column
			} else if(m - 1 >= 0 && l + 1 >= images[k].height
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m - 1].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m - 1].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m - 1].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

			//if above is a match and you're in the first row
			} else if (m - 1 < 0 && l + 1 < images[k].height
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

			}
			//If this pixel has an equal area to the max pixel and it's further right or further up, make it the max
			if((images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].x > max.max_pix.x)
					|| (images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].y > max.max_pix.y)) {

				max.max_pix = images[k].pixArray[images[k].width * l + m];

			}
			//If the area at this pixel is greater than the current largest pixel, make the max pixel be this pixel
			if(images[k].pixArray[images[k].width * l + m].area > max.max_pix.area) {
				max.max_pix = images[k].pixArray[images[k].width * l + m];
			}
		}
	}

	//reset all the pixels back to original values
	for(l = 0; l < images[k].height; l++) {
		for(m = 0; m < images[k].width; m++) {
			images[k].pixArray[images[k].width * l + m].area = 1;
			images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
			images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
		}
	}
	//scan the picture from the bottom right
	for(l = images[k].height - 1; l >= 0; l--) {
		for(m = images[k].width - 1; m >= 0 ; m--) {
			//If the pixel to the left and below are the same color
			if(m + 1 < images[k].width && l + 1 < images[k].height) {

				//if the color matches below and behind
				if(images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

					if(images[k].pixArray[images[k].width * (l + 1) + m].area <= images[k].pixArray[images[k].width * l + m + 1].area){
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					}


					//if behind is not a match, but below is
				} else if (images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * l + m + 1].color
						&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

					//if the starting_x of the one below is not less than the current x position
					if(images[k].pixArray[images[k].width * (l + 1) + m].start_x == images[k].pixArray[images[k].width * l + m].x) {
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					}

					//if behind is a match, but below is not and the start_y of the one behind equal to the current y postition
				} else if (images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color
						&& images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * (l + 1) + m].color
						&& images[k].pixArray[images[k].width * l + m + 1].start_y == images[k].pixArray[images[k].width * l + m].y) {

					//we are in the same rectangle, so we have the same starting point
					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

				} else {

					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

				}

			//if the pixel to the right is a match and you're in the last column
			} else if(m + 1 < images[k].width && l + 1 >= images[k].height
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

			//if the pixel above is a match and you're in the last row
			} else if (m + 1 >= images[k].width && l + 1 < images[k].height
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l + 1) + m].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l + 1) + m].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l + 1) + m].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);

			}
			//If this pixel has an equal area to the max pixel and it's further right or further up, make it the max
			if((images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].x > max.max_pix.x)
					|| (images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].y > max.max_pix.y)) {

				max.max_pix = images[k].pixArray[images[k].width * l + m];

			}
			//If the area at this pixel is greater than the current largest pixel, make the max pixel be this pixel
			if(images[k].pixArray[images[k].width * l + m].area > max.max_pix.area) {
				max.max_pix = images[k].pixArray[images[k].width * l + m];
			}
		}
	}

	//reset all the pixels back to original values
	for(l = 0; l < images[k].height; l++) {
		for(m = 0; m < images[k].width; m++) {
			images[k].pixArray[images[k].width * l + m].area = 1;
			images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
			images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
		}
	}
	//scan the picture from the bottom right
	for(l = 0; l < images[k].height; l++) {
		for(m = images[k].width - 1; m >= 0 ; m--) {
			//If the pixel to the left and below are the same color
			if(m + 1 < images[k].width && l - 1 >= 0) {

				//if the color matches below and behind
				if(images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

					if(images[k].pixArray[images[k].width * (l - 1) + m].area <= images[k].pixArray[images[k].width * l + m + 1].area){
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);
					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);
					}


					//if behind is not a match, but below is
				} else if (images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * l + m + 1].color
						&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

					//if the starting_x of the one below is not less than the current x position
					if(images[k].pixArray[images[k].width * (l - 1) + m].start_x == images[k].pixArray[images[k].width * l + m].x) {
						//we are in the same rectangle, so we have the same starting point
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

					} else {
						images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
						images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
						images[k].pixArray[images[k].width * l + m].area =
								abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
								* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y - 1);
					}

					//if behind is a match, but below is not and the start_y of the one behind equal to the current y postition
				} else if (images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color
						&& images[k].pixArray[images[k].width * l + m].color != images[k].pixArray[images[k].width * (l - 1) + m].color
						&& images[k].pixArray[images[k].width * l + m + 1].start_y == images[k].pixArray[images[k].width * l + m].y) {

					//we are in the same rectangle, so we have the same starting point
					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

				} else {

					images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m].x;
					images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m].y;
					images[k].pixArray[images[k].width * l + m].area =
							abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
							* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

				}

			//if the pixel to the right is a match and you're in the last column
			} else if(m + 1 < images[k].width && l - 1 < 0
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * l + m + 1].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * l + m + 1].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * l + m + 1].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x - 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

			//if the pixel above is a match and you're in the last row
			} else if (m + 1 >= images[k].width && l - 1 >= 0
					&& images[k].pixArray[images[k].width * l + m].color == images[k].pixArray[images[k].width * (l - 1) + m].color) {

				//we are in the same rectangle, so we have the same starting point
				images[k].pixArray[images[k].width * l + m].start_x = images[k].pixArray[images[k].width * (l - 1) + m].start_x;
				images[k].pixArray[images[k].width * l + m].start_y = images[k].pixArray[images[k].width * (l - 1) + m].start_y;
				images[k].pixArray[images[k].width * l + m].area =
						abs(images[k].pixArray[images[k].width * l + m].x - images[k].pixArray[images[k].width * l + m].start_x + 1)
						* abs(images[k].pixArray[images[k].width * l + m].y - images[k].pixArray[images[k].width * l + m].start_y + 1);

			}
			//If this pixel has an equal area to the max pixel and it's further right or further up, make it the max
			if((images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].x > max.max_pix.x)
					|| (images[k].pixArray[images[k].width * l + m].area == max.max_pix.area
					&& images[k].pixArray[images[k].width * l + m].y > max.max_pix.y)) {

				max.max_pix = images[k].pixArray[images[k].width * l + m];

			}
			//If the area at this pixel is greater than the current largest pixel, make the max pixel be this pixel
			if(images[k].pixArray[images[k].width * l + m].area > max.max_pix.area) {
				max.max_pix = images[k].pixArray[images[k].width * l + m];
			}

		}
	}

	//fill the ImageInfo struct with coordinates
	if(max.max_pix.start_x < max.max_pix.x) {
		analyzedImages[k].bottom_right_x = max.max_pix.x;
		analyzedImages[k].top_left_x = max.max_pix.start_x;
	} else {
		analyzedImages[k].bottom_right_x = max.max_pix.start_x;
		analyzedImages[k].top_left_x = max.max_pix.x;
	}
	//these y-coordinates are based on the coordinate system starting in the upper left corner
	//All the rest of the math was done as though we would start in the lower right so we
	//simply subtract the y position from the height - 1
	if(max.max_pix.start_y < max.max_pix.y) {
		analyzedImages[k].bottom_right_y = images[k].height - 1 - max.max_pix.start_y;
		analyzedImages[k].top_left_y = images[k].height - 1 - max.max_pix.y;
	} else {
		analyzedImages[k].bottom_right_y = images[k].height - 1 - max.max_pix.y;
		analyzedImages[k].top_left_y = images[k].height - 1 - max.max_pix.start_y;
	}
}

/**
 * This thread iterates through the images for this thread and analyzes them for the largest
 * rectangle.
 */
void image_analysis_thread(bmp_analyze * analyze_info) {
	int j;
	for(j = analyze_info->k; j < analyze_info->i; j += analyze_info->thread_limit) {
		analyze_bmp_now(analyze_info->analyzedImages, analyze_info->images, j);
	}
}

/**
 * Reads in all the bitmap files in a directory and returns ImageInfo on each one
 */
struct ImageInfo * analyze_images_in_directory(int thread_limit, const char * directory, int * images_analyzed) {


	//we want an array of images where we store the image data
	struct image *images;
	images = (struct image *)malloc(50 * sizeof(struct image));

	//an array of analyzed images that contains the file path and the coordinates of the largest rectangle of a single color
	struct ImageInfo *analyzedImages;
	analyzedImages = (struct ImageInfo *)malloc(50 * sizeof(struct ImageInfo));


	int i, k;

	i = 0;
//************************************************************
//**********Threading starts here*****************************
//************************************************************

	//populate argument for bmp copy
	BMPcopy copy;
	copy.images = images;
	copy.directory = (char *)malloc(200 * sizeof(char));
	while(directory[i] != '\0'){
		copy.directory[i] = directory[i];
		i++;
	}
	copy.directory[i] = '\0';

	//start thread for bmp copy
	pthread_t copy_bmp;
	if(pthread_create(&copy_bmp, NULL, copyThread, &copy) != 0) {
		printf("ERROR: Unable to create new thread.\n");
		exit(EXIT_FAILURE);
	}

	pthread_join(copy_bmp, NULL);

	free(copy.directory);

	//analyze the images in multiple threads
	bmp_analyze analyze_info[thread_limit];

	pthread_t analyze_thread[thread_limit];

	for(k = 0; k < thread_limit; k++) {

		analyze_info[k].analyzedImages = analyzedImages;
		analyze_info[k].i = num_bmps;
		analyze_info[k].images = images;
		analyze_info[k].thread_limit = thread_limit;
		analyze_info[k].k = k;
		if(pthread_create(&analyze_thread[k], NULL, image_analysis_thread, &analyze_info[k]) != 0) {
			printf("ERROR: Unable to create new thread.\n");
			exit(EXIT_FAILURE);
		}

	}

	for(k = 0; k < thread_limit; k++) {
		pthread_join(analyze_thread[k], NULL);
	}

	/*int l, m;

	for(k = 0; k < i; k ++) {
		for(l = images[k].height - 1; l >= 0; l--) {
			for(m =  0; m < images[k].width ; m++) {
				printf("%x", 0x00000f & images[k].pixArray[images[k].width * l + m].color);
			}
			printf("\n");
		}
		printf("\n");
	}*/
	*images_analyzed = num_bmps;
	return analyzedImages;
}


