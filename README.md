# README #

### What is this repository for? ###

* Program that scans a directory for bitmap images in one thread, and, in multiple threads, analyzes each image to find the largest rectangle of pixels of uniform color.
* Version 1

### How do I get set up? ###

* One of the input arguments for main.c needs to be set to the directory in which you have bitmap images you wish to analyze.
* Run main.c.

### Authors ###

* Christopher Helmer
* Matt Alden

### Who do I talk to? ###

* Christopher Helmer
* christopher.helmer@yahoo.com